import sys

from garbageBot import GitlabServer


if __name__ == '__main__':
    gls = GitlabServer()
    docker_registry = gls.get_registry_info()
    gls.scope_filter_and_delete(docker_registry)
